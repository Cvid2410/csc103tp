/*
 * CSc103 Project 1: (hello_world++)
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References: Your aid, the book, and a bit of experience in assembly language that I have.
 * SHA1: 0ca3e555a56b7e1ea766d386fafb727775a16ead
 * Receipt: 6d75ffb6b8ca07e3644494fbabf48f8fade6f354
 */

#include <iostream>
using std::cin;
using std::cout;
#include <string>
using std::string;

int main()

{
    string name, relative;
    
    cout<<"Enter your Las name:\n";
    getline(cin,name);
    
    cout<<"Enter a relative:\n";
    getline(cin,relative);
    
    cout<<"Hello. My name is "<<name + ". You killed my " <<relative +". Prepare to die.\n";
}