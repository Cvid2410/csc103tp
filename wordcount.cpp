/*
 * CSc103 Project 3: wordcount++
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References: Pg 392, 121 from the book <- the book is what helped us the most!
 * Youtube Channels: Thenewboston, codingmadeeasy
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours: like 48 hours, and pullled out an all nighter!
 */

#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <string>
using std::string;
#include <set>
using std::set;
#include <sstream>
using std::stringstream;

// write this function to help you out with the computation.

/*
unsigned long countLines(string& s, set<string>& wl, int& count)
{
    while(getline(cin,s)&& s!="quit"){
        wl.insert(s);
        count++;
    }
    return 0;
}
*/

int main()
{
    set<string> wordLine;
    set<string> wordList;
    
    string line = "";
    string word = "";
    
    int nofLines=0;
    int nofWords=0;
    int nofChars=0;
   
    while(getline(cin,line)&& line!="quit" ){
    for (int i = 0; i<line.length(); i++) {
    nofChars++;
    }
    
    wordLine.insert(line);
    nofLines++;
        
        
    stringstream ss(line);
    while (ss>>word){
    wordList.insert(word);
    nofWords++;
    }
        
    }
    
    cout<<nofLines<<"\t"<<nofWords<<"\t"<<nofChars+nofLines<<"\t"<<wordLine.size()<<"\t"<<wordList.size()<<endl;
    
   
 /*
    
    for (set<string>::iterator i = wordLine.begin(); i!=wordLine.end(); i++) {
        cout << *i << endl;
        
    }
    
    countLines(line,wordLine,nofLines);
     
    cout<<"Number of unique lines is :"<<wordLine.size()<<endl;
    cout<<"Number of lines is :"<<nofLines<<endl;
    cout<<"Number of unique words is:"<<wordList.size()<<endl;
    cout<<"Number of words is :"<<nofWords<<endl;
    cout<<"Number of characters is:"<<nofChars+nofLines<<endl;
    
    for (set<string>::iterator i = wordList.begin(); i!=wordList.end(); i++) {
        cout << *i << endl;
     
    }
    
    */
	
    return 0;
}




